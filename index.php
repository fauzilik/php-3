<?php

require_once('animal.php');
require_once('ape.php');


$hewan = new Animal("Shaun");
echo "Name : " . $hewan->Name .  "<br>";
echo "Legs : " . $hewan->Legs .  "<br>";
echo "Cold_blooded : " . $hewan->Cold_blooded .  "<br> <br>";

$kera = new Ape("Kera");
echo "Name : " . $kera->Name .  "<br>";
echo "Legs : " . $kera->Legs .  "<br>";
echo "Cold_blooded : " . $kera->Cold_blooded .  "<br>";
echo "Yell : " . $kera->Yell .  "<br> <br>";



?>